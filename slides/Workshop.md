---
theme : "black"
transition: "slide"
highlightTheme: "nord"
logoImg: "https://cdn1.homeadvisor.com/images/logos/ha_big_logo.png"
slideNumber: true
---

# Reusable Components Workshop

---

## Whoami?

<img src="http://localhost:8080/assets/profile.png" />

note: name is Isaac, senior software developer here at homeadvisor, been professionally programming for about 7 years now, programming for about 14. I've only been using Vue though for about 10 months, so still pretty new at it. Most of the content of this talk is being pulled from the Rediculously Reusable Components workshop at VueConf 2019 that I was able to attend. HomeAdvisor sent 4 of us to the conference and when we got back we put together a few workshops to share the knowledge with the rest of our front-end developers.

---

## Goal

To learn to implement dynamic, reusable, and maintainable components.

---

## Quick Survey

note: Quick survey to see where we should spend most of our time and what we can reasonably speed through. Raise hands if you know how to use (1) slots. Keep your hands up if you know (2) named slots. (3) Scoped Slots. (4) Dynamic Slots. (5) Events (6) Scoped CSS (7) Module CSS

---

## How

1. Explanation of concepts - 10 - 15 min <!-- .element: class="fragment fade-in" -->
2. Application - 35-40 min <!-- .element: class="fragment fade-in" -->
   1. Try the exercise <!-- .element: class="fragment fade-in" -->
   2. Learn a technique <!-- .element: class="fragment fade-in" -->
   3. Try again <!-- .element: class="fragment fade-in" -->
   4. One possible solution <!-- .element: class="fragment fade-in" -->
3. Questions <!-- .element: class="fragment fade-in" -->

note: (1) How do we reach that goal? (2) I'll give a brief explanation of some concepts and then we'll work through some exercises to learn how to apply those concepts. (3) Each exercise has been started for you and there should be a comment where you need to put your code. In each exercise you'll have to create or modify a component to make it work the way it's being called from the exercise itself. (4) Once you've given it a try, I'll present a technique that can help you solve the problem. (5) You'll have a couple more minutes to apply the technique, (6)and then we'll briefly go over one possible solution. There are many ways of solving these exercises, and by no means am I saying that my solution is the best. (7) Finally we'll end with some time for questions.

---

## Rules

### There are none! <!-- .element: class="fragment fade-in" -->

* Ask questions <!-- .element: class="fragment fade-in" -->
* Talk to your neighbors <!-- .element: class="fragment fade-in" -->
* Checkout the solutions <!-- .element: class="fragment fade-in" -->
* Learn how you learn best <!-- .element: class="fragment fade-in" -->

note: There really are no rules. Any question you ask is likely to be the same thing someone else is wondering. Collaboration is a great way to learn and honestly, I want everyone to get the most out of these few minutes as they can. You know how you learn best, so do what serves you best.

---

## Getting Started

1. Clone
   * `git@gitlab.com:idoub/vue-components-workshop.git`
2. Install
   * `npm i && npm run serve`
3. Practice
   * Exercises are outlined on `localhost:8080`
   * Solutions are on the `solutions` branch

<a href="#/9" style="float: right; font-size: 10px;">link</a>

---

## The Problem?

note: what is the problem we're trying to solve and how can components solve these problems?

--

### This

<figure>
  <img src="http://localhost:8080/assets/vue.png" height="500px"/>
  <figcaption style="font-size: 14px; font-style: italic;">button.vue</figcaption>
</figure>

note: this is a vue implementation of a button in our system

--

### Isn't much better than this

<figure style="max-width: 80%; display: inline-block">
  <img src="http://localhost:8080/assets/asyncha.png" height="270px" />
  <figcaption style="font-size: 14px; font-style: italic;">button.pug</figcaption>
</figure>
<figure style="max-width: 15%; display: inline-block">
  <img src="http://localhost:8080/assets/asynchajs.png" height="270px" />
  <figcaption style="font-size: 14px; font-style: italic;">button.js</figcaption>
</figure>

note: this is a previous implementation of a button in our homegrown framework

--

### Solution?

--

<img src="http://localhost:8080/assets/button.png" />

Leverage the features of Vue to write loosly coupled components. <!-- .element: class="fragment fade-in" -->

note: this is what we should have written

---

## Principles to Keep in Mind

note: before we dive into implementation details, let's look at some high level concepts that will help drive well designed components.

--

### KISS

#### Keep it Simple Stupid <!-- .element: class="fragment fade-in" -->

<p class="fragment fade-in"><q>Simplicity is the ultimate sophistication</q> - Leonardo da Vinci</p>

<p class="fragment fade-in"><q>Brevity is the soul of wit</q> - William Shakespeare</p>

<p class="fragment fade-in"><q>It seems that perfection is reached not when there is nothing left to add, but when there is nothing left to take away</q> - Antoine de Saint Exupéry</p>

note: Coined by Kelly Johnson, lead engineer at the Lockheed Skunk Works back in 1960, the KISS principle states that most systems work best if they are kept simple rather than made complicated; therefore, simplicity should be a key goal in design, and unnecessary complexity should be avoided. This principle has been espoused for hundreds of years in lots of different forms. Leonardo da Vinci, William Shakespeare, ahn-twan duh san-teg-zy-pey-ree author of "The Little Prince".

--

### YAGNI

#### You Ain't Gonna Need It <!-- .element: class="fragment fade-in" -->

<p class="fragment fade-in"><q>Always implement things when you actually need them, never when you just foresee that you need them.</q> - Ron Jeffries, Extreme Programming co-founder</p>

note: Almost identical to the KISS principle is YAGNI. You ain't gonna need it. You may have also heard this princple expressed in the phrase "do the simplest thing that could possibly work". Humans are notoriously bad a predicting the future. So don't. Just Don't. This is similar to the concept of Postponement in supply chain management theory which is where the manufacturer produces a generic product, which can be modified at the later stages before the final transport to the customer. So producing all white umbrellas that can later be dyed according to the colors that are in greatest demand.

--

### Other Principles to Learn

**Single Responsibility Principle** <!-- .element: class="fragment fade-in" -->
<span class="fragment fade-in" style="font-size: 16px; font-style: italic;">Robert C. Martin (Uncle Bob)</span>

**Separation of Concerns** <!-- .element: class="fragment fade-in" -->
<span class="fragment fade-in" style="font-size: 14px; font-style: italic;">Edsger W. Dijkstra</span>

**Open Closed Principle** <!-- .element: class="fragment fade-in" -->
<span class="fragment fade-in" style="font-size: 14px; font-style: italic;">Bertrand Meyer</span>

note: (1) The Single Responsibility Principle states that every module, class or function should have responsibility over a single piece of *functionality* in the code and should *entirely* encapsulate that funcationlity. (2) Similarly, Separation of Concerns says each piece of the software shouldn't care about the other pieces, their context or data. Following this leads to *modular* code and is acheived with encapsulation. (3) Encapsulation refers to restricting access to data or bundling data with the methods where it is used instead of letting it bleed across your program. (4) The Open Closed Principle says that functions, modules, classes, components etc should be open for extension but closed for modification. (5) To give a solid example, once you've written a Button.vue component, you shouldn't ever edit the file again. Instead, if you want to add functionality, you should create a wrapper component around your button and add that functionality there. To follow this principle, you need to write your components in such a way that they are flexible and reusable *without* violating any of the other principles. (6) Let's get a bit more practicle.

---

## Simple Slots

```html
<slot></slot>
```

note: We'll start with the slot directive.

--

<span style="font-size: 14px; font-style: italic;">index.vue</span>

```html
<form>
  <my-button>Click Me!</my-button>
</form>
```
<!-- .element: class="fragment fade-in-then-semi-out" -->

<span style="font-size: 14px; font-style: italic;">MyButton.vue</span>

```html
<template>
  <button class="btn">
    <slot></slot>
  </button>
</template>
```
<!-- .element: class="fragment fade-in" -->

note: You can create components that accept content or other components by using a slot. This will be rendered to ...

--

<span style="font-size: 14px; font-style: italic;">out.html</span>

```html
<form>
  <button class="btn">Click Me!</button>
</form>
```

--

<span style="font-size: 14px; font-style: italic;">MyButton.vue</span>

```html
<template>
  <button class="btn">
    <slot>Submit</slot>
  </button>
</template>
```

note: slots can also define fallback content

--

<span style="font-size: 14px; font-style: italic;">index.vue</span>

```html
<form>
  <my-button></my-button>
</form>
```
<!-- .element: class="fragment fade-in-then-semi-out" -->

<span style="font-size: 14px; font-style: italic;">out.html</span>

```html
<form>
  <button class="btn">Submit</button>
</form>
```
<!-- .element: class="fragment fade-in" -->

note: so this -> is rendered to this

--

<span style="font-size: 14px; font-style: italic;">MyButton.vue</span>

```html
<template>
  <button class="btn">
    <slot name="icon"></slot> <!-- Named Slot -->
    <slot>Submit</slot> <!-- Default Slot -->
  </button>
</template>
```

note: You can also give slots a name so a component can have multiple slots ... (after back) By implementing the icon like this I'm telling the button component to *care* about how it's going to be used. It's presenting to all the callers "Hey, you can either have an icon and text or just text." (go down 2)

--

<span style="font-size: 14px; font-style: italic;">index.vue</span>

```html
<form>
  <my-button class="btn">
    <template v-slot:icon>
      <i class="fas fa-asterisk"></i>
    </template>
    <template v-slot:default> <!-- Optional "template" -->
      Submit
    </template>
  </my-button>
</form>
```

note: Then you reference the slots like this. but what am I violating here? (Go back)

--

<span style="font-size: 14px; font-style: italic;">index.vue</span>

```html
<form>
  <my-button class="btn">
    <template v-slot:icon>
      <i class="fas fa-asterisk"></i>
    </template>
    <template v-slot:default>
      Submit
    </template>
    <template v-slot:icon>
      <i class="fas fa-arrow-right"></i>
    </template>
  </my-button>
</form>
```

note: but what if I wanted a button with two icons?

--

<span style="font-size: 14px; font-style: italic;">MyButton.vue</span>

```html
<template>
  <button class="btn">
    <slot name="icon"></slot>
    <slot>Submit</slot>
    <slot name="right-icon"></slot> <!-- modification -->
  </button>
</template>
```

note: now I'd have to modify the original button component, violating the open closed principle.

--

<span style="font-size: 14px; font-style: italic;">MyButton.vue</span>

```html
<template>
  <button class="btn">
    <slot>Submit</slot>
  </button>
</template>
```

note: the better option is to leave the button component as it was

--

<span style="font-size: 14px; font-style: italic;">index.vue</span>

```html
<form>
  <my-button class="btn">
    <!-- This will ALL be inserted into the slot -->
    <i class="fas fa-asterisk"></i>
    <span>Submit</span>
    <i class="fas fa-arrow-right"></i>
  </my-button>
</form>
```

note: and either let the parent decide how they want to call it

--

<span style="font-size: 14px; font-style: italic;">TwoIconButton.vue</span>

```html
<template>
  <my-button class="btn">
    <i class="fas fa-asterisk"></i>
    <slot></slot>
    <i class="fas fa-arrow-right"></i>
  </my-button>
</template>
```

note: or implement the modification in a wrapper

--

<span style="font-size: 14px; font-style: italic;">MyProfile.vue</span>

```html
<template>
  <slot>
    <span>{{ profile.username }}</span>
  </slot>
</template>
```
<!-- .element: class="fragment fade-in-then-semi-out" -->

<span style="font-size: 14px; font-style: italic;">index.vue</span>

```html
<template>
  <my-profile>
    <img :src="profile.imageURL" />
<!-- This will error, we don't know what "profile" is. -->
    <span>{{ profile.firstname }}</span>
  </my-profile>
</template>
```
<!-- .element: class="fragment fade-in" -->

note: Now one problem you'll quickly run into is that the component has context of the parent, but the parent has no context of the child component. So if the parent wants to replace fallback content with data that is *scoped* to the child component, you need another tool.

--

<span style="font-size: 14px; font-style: italic;">MyProfile.vue</span>

```html
<template>
  <slot v-bind="profile">
    <span>{{ profile.username }}</span>
  </slot>
</template>
```
<!-- .element: class="fragment fade-in-then-semi-out" -->

<span style="font-size: 14px; font-style: italic;">index.vue</span>

```html
<template>
  <my-profile>
    <template v-slot="profile">
      <img :src="profile.imageURL" />
      <span>{{ profile.firstname }}</span>
    </template>
  </my-profile>
</template>
```
<!-- .element: class="fragment fade-in" -->

note: you can solve this by passing scoped properties from the component back up to the parent and binding them there using values on the "v-bind" and "v-slot" attributes. You should use the Template tag when scoping attributes even when there's only a single slot in the component.

---

## Exercises

*Let's start applying stuff!* <!-- .element: class="fragment fade-in" -->

<a href="#/5" style="float: right; font-size: 10px;">link</a>

note: Ok, introduction to slots is over, let's start applying stuff with some exercises.

---

## Exercise 1

Create a *`List.vue`* component that can render both of the following lists.

<figure class="fragment fade-in">
  <img src="http://localhost:8080/assets/list.png" height="300px"/>
  <figcaption style="font-size: 14px; font-style: italic;">button.vue</figcaption>
</figure>

--

### Demonstration

<iframe src="http://localhost:8080/1" height="500px" width="800px"></iframe>

--

### Solution

```html
<template>
  <ul>
    <li v-for="(item, i) in items" v-bind:key="i" >
      <slot v-bind="{item, i}">{{ item }}</slot>
    </li>
  </ul>
</template>
```

---

## Exercise 2

Create a *`Button.vue`* component that changes the parent text when clicked.

--

### Demonstration

<iframe src="http://localhost:8080/2" height="500px" width="800px"></iframe>

--

### Solution

```html
<template>
  <button @click="buttonClick">
    <slot></slot>
  </button>
</template>
```
<!-- .element: class="fragment fade-in-then-semi-out" -->

```js
export default {
  name: 'Button',
  methods: {
    buttonClick() {
      this.$emit('button-click');
    }
  }
}
```
<!-- .element: class="fragment fade-in" -->

---

## Exercise 3

Make sure that the previous button can modify other sibling components when clicked.

--

### Demonstration

<iframe src="http://localhost:8080/3" height="500px" width="800px"></iframe>

--

### Solution

```html
<template>
  <button @click="buttonClick">
    <slot></slot>
  </button>
</template>
```
<!-- .element: class="fragment fade-in-then-semi-out" -->

```js
export default {
  name: 'Button',
  methods: {
    buttonClick() {
      this.$emit('button-click');
    }
  }
}
```
<!-- .element: class="fragment fade-in" -->

---

## Exercise 4

*"Without modifying the previous button component, create a third component* *`StateChange.vue`* *that when rendered inside a button and clicked will cycle through `initial`, `transition` and `final` states. The button will spend 1 second on the `transition` state and if clicked during the `transition` state will show an `error` state."*

All of the code for the state-change requirements has already been provided. You just need to tie it in so the state is controlled by the parent.

--

### Demonstration

<iframe src="http://localhost:8080/4" height="500px" width="800px"></iframe>

--

### Solution

```js
  created() {
    this.$emit('expose-handler', this.changeState);
  }
```
<!-- .element: class="fragment fade-in" -->

note: This is a bit of an odd pattern initially, but works because it maintains encapsulation and separation of concerns. The child doesn't care how it's used, it just exposes it's functionality as an API. The parent can tie into that API if it wants, but it doesn't have to and nothing breaks. The next exercise really isn't an exercise, it just demonstrates the flexibility of this pattern by swapping the parent and child so that we're rendering buttons and text inside of the state-change component.

---

## Exercise 5

Now swap the components so that buttons are rendered inside the *`StateChange.vue`* component for the initial and error states and plain text is shown for the transition and final states.

--

### Demonstration

<iframe src="http://localhost:8080/5" height="500px" width="800px"></iframe>

--

### Solution

```js
  created() {
    this.$emit('expose-handler', this.changeState);
  }
```
<!-- .element: class="fragment fade-in" -->

note: the same as the previous exercise.

---

## Recap Slots

1. <span class="fragment fade-in" style="font-size: 0.8em">Slots - `<slot></slot>`</span>
2. <span class="fragment fade-in" style="font-size: 0.8em">Fallback Content - `<slot>fallback content</slot>`</span>
3. <span class="fragment fade-in" style="font-size: 0.8em">Scoped Slots - `<slot v-bind:key="{value}"></slot>`</span>
4. <span class="fragment fade-in" style="font-size: 0.8em">Named Slots - `<slot name="name">`</span>
5. <span class="fragment fade-in" style="font-size: 0.8em">Events - `this.$emit('eventName', obj)`</span>
6. <span class="fragment fade-in" style="font-size: 0.8em">Exposed Handler Pattern = `this.$emit('eventName', handlerFunction)`</span>

---

## What we *didn't* cover

1. Transparent components <!-- .element: class="fragment fade-in" -->
```html
v-bind="{...$attrs, ...$props}" v-on="$listeners"
```
<!-- .element: class="fragment fade-in" -->

1. Functional components <!-- .element: class="fragment fade-in" -->
```html
v-bind="state" -> v-bind="props.state"
```
<!-- .element: class="fragment fade-in" -->

1. Dynamic components <!-- .element: class="fragment fade-in" -->
```html
<component v-bind:is="componentType"></component>
```
<!-- .element: class="fragment fade-in" -->

1. Async components <!-- .element: class="fragment fade-in" -->
2. Mixins <!-- .element: class="fragment fade-in" -->

---

## Vue Provides a LOT of Tools!

*Moving on to CSS*

---

## Exercise 6

Make a *`Link.vue`* component that has a red link instead of the default blue and is italicised.

--

### Demonstration

<iframe src="http://localhost:8080/6" height="500px" width="800px"></iframe>

--

### Solution

```html
<style scoped>
a.link { ... }
</style>
```

---

## Exercise 7

Make the *`Link.vue`* component *always* red, even when there are randomly applied styles you can't change.

--

### Demonstration

<iframe src="http://localhost:8080/7" height="500px" width="800px"></iframe>

--

### Solution

```html
<style module>
a.link { ... }
</style>
```
<!-- .element: class="fragment fade-in-then-semi-out" -->

```html
<a :class="$style.link" href="#">
```
<!-- .element: class="fragment fade-in" -->

---

## Questions?

---

## Feedback

https://www.surveymonkey.com/r/DX9XHRY

<img src="http://localhost:8080/assets/qr.png" height="450px" />
