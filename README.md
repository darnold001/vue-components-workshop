# Reusable 

## Running the workshop

Run `npm i && npm run serve` on the command line and then navigate to [http://localhost:8080/](http://localhost:8080/) to view the working website.

## Viewing the slides

The slides are included in this repository and are built with [reveal.js](https://revealjs.com/). The easiest way to run them is to install the [vscode-reveal](https://marketplace.visualstudio.com/items?itemName=evilz.vscode-reveal) plugin and run the slides directly from VSCode.

## Getting the Solutions

The solutions are on the `solutions` git branch. In order to use them, you'll have to change the import paths in the exercises to point to `/solutions` instead of `/components`.
